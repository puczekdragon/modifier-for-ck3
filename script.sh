#!/bin/bash
pushd ..
for d in */; do
	if [[ -f "${d}descriptor.mod" ]] && [[ ! -f "${d}modallredypackedout" ]]
	then
		#this part create .mod file
		content=$(<"${d}descriptor.mod")
		modname=$(cat ${d}descriptor.mod | grep name | cut -c 7-)
		modname=${modname%?}
		filename="${modname}.mod"
		echo "$content" >> "$filename"
		sed -i '$ d' "$filename"
		echo "path=\"mod/${modname}\"" >> "$filename"
		#this part moves mod files to new folder
		mkdir "${modname}"
		temp="$d"
		mv ${d}/* "${modname}"
		rmdir "${temp}"
		#adding temporary temp file to not run on allready created folders
		touch "${modname}/modallredypackedout"
	fi
done
popd