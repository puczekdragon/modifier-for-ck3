## How to use

1. Go to [Crusader Kings III workshop](https://steamcommunity.com/app/1158310/workshop/) and pick mods you want.
2. Get links of your picked mods and download them via [workshop downloader](https://steamworkshopdownloader.io/).
3. Move all zipped downloaded mods to C:\Users\[your username]\Documents\Paradox Interactive\Crusader Kings III\mod\ or other location on difrent systems. 

![before unzip](img/before-unzip.png "Before unzip, don't mind temp folder")

4. Unzip all to separate folders. 

![before script](img/before-script.png "Before script, don't mind temp folder")

5. Run script as super user from folder mod/modifier-for-ck3, you can use [Cygwin](https://www.cygwin.com/), [WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10) or [bash on windows](https://www.geeksforgeeks.org/use-bash-shell-natively-windows-10/).

![after script](img/after-script.png "After script, don't mind temp folder")

6. Open CK3 launcher (dowser.exe in launcher folder).
7. Go to Mods -> Manage Mods -> Add More Mods, there select new mods and click "Add To Playset"
8. Enable them and play the game!


#### Non bash script
In future, I want to add non-bash script to run it natively on windows, but everything at its time.
